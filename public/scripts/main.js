"use strict";

// Đăng nhập
function signIn() {
	// Đăng nhập firebase với xác thực Google
	var provider = new firebase.auth.GoogleAuthProvider();
	firebase.auth().signInWithPopup(provider);
}

// Lưu user sau khi đăng nhập
function updateUserOnline(id, user) {
	var url = "/userOnline/" + id;
	firebase.database().ref(url).set({
		...user,
		date: new Date().getTime()
	}).catch(function (error) {
		console.error("Error writing new message to Realtime Database:", error);
	});
}

// Lấy danh sách user đang hoạt động
getAllUserOnline();
function getAllUserOnline() {
	firebase.database().ref("/userOnline/").on("value", function (data) {
		listUserOnline.innerHTML = "";
		data = JSON.stringify(data);
		data = JSON.parse(data);
		listUserOnline.innerHTML = "";
		renderListUserOnline(data);
		function renderListUserOnline(listUser) {
			for (var key in data) {
				var user = data[key];
				if(!!user){
					if(user.email && covertEmailId(user.email) !== getEmailId()){
						listUserOnline.appendChild(renderItemUser(user));
					}
				}
			}
		}
		// Tạo một dòng hiển thị người dùng
		function renderItemUser(user) {
			var mainPanel = document.createElement('div');
			mainPanel.classList.add('item');
				var avatar = document.createElement('img');
				avatar.setAttribute('src', user.avatar);
				var info = document.createElement('div');
					var userName = document.createElement('div');
					userName.innerHTML = user.userName;
					var date = document.createElement('div');
					date.style.fontSize = "12px";
					date.innerHTML = timeDifference(new Date().getTime(), user.date);
					info.appendChild(userName);
					info.appendChild(date);
				mainPanel.appendChild(avatar);
				mainPanel.appendChild(info);
			return mainPanel;
		}
	}, function (err) {
		console.log("err ", err);
	});
}

// Đăng xuất
function signOut() {
	var url = "/userOnline/" + getEmailId();
	firebase.database().ref(url).remove();
	firebase.auth().signOut();
}

// Khởi tạo xác thực Firebase
function initFirebaseAuth() {
	firebase.auth().onAuthStateChanged(authStateObserver);
}

// Lấy ảnh của người dùng
function getProfilePicUrl() {
	return (firebase.auth().currentUser.photoURL || "/public/images/profile_placeholder.png");
}

// Lấy Tên hiển thị của người dùng
function getUserName() {
	return firebase.auth().currentUser.displayName;
}

// Lấy id từ email
function getEmailId() {
	var currentUser = firebase.auth().currentUser;
	return currentUser ? covertEmailId(currentUser.email) : null;
}

// Chuyển đổi email thành id
function covertEmailId(email) {
	return email ? email.replace(/[.*+?^${}()|[\]\\@]/g, '_') : null;
}
// Lấy email đăng ký của người dùng
function getEmail() {
	return firebase.auth().currentUser.email;
}
// Kiểm tra user có đang đăng nhập
function isUserSignedIn() {
	return !!firebase.auth().currentUser;
}

// Tải về các tin nhắn cũ
function loadMessages() {
	var callback = function (snap) {
		var data = snap.val();
		displayMessage( snap.key, data.name, data.text, data.profilePicUrl, data.imageUrl , data.id , data.date);
	};
	firebase.database().ref("/messages/").limitToLast(12).on("child_added", callback);
	firebase.database().ref("/messages/").limitToLast(12).on("child_changed", callback);
}

//  Gửi một tin nhắn lên db
function saveMessage(messageText) {
	return firebase.database().ref("/messages/").push({
		id: getEmailId(),
		name: getUserName(),
		text: messageText,
		profilePicUrl: getProfilePicUrl(),
		date: new Date().getTime(),
	})
	.catch(function (error) {
		console.error("Error writing new message to Realtime Database:", error);
	});
}

// Gửi một ảnh bằng một tin nhắn
function saveImageMessage(file) {
	firebase
		.database()
		.ref("/messages/")
		.push({
			id: getEmailId(),
			name: getUserName(),
			imageUrl: LOADING_IMAGE_URL,
			profilePicUrl: getProfilePicUrl(),
			date: new Date().getTime(),
		})
		.then(function (messageRef) {
			// 2 - Tải lên ảnh
			var filePath = firebase.auth().currentUser.uid + "/" + messageRef.key + "/" + file.name;
			return firebase.storage().ref(filePath).put(file)
				.then(function (fileSnapshot) {
					// 3 - Tạo url ảnh
					return fileSnapshot.ref.getDownloadURL().then(url => {
						// 4 - Lưu nội dung tun nhắn và url ảnh lên db
						return messageRef.update({
							imageUrl: url,
							storageUri: fileSnapshot.metadata.fullPath
						});
					});
				});
		})
		.catch(function (error) {
			console.error(
				"There was an error uploading a file to Cloud Storage:",
				error
			);
		});
}

function saveMessagingDeviceToken() {
	// TODO 10: Lưu lại mã thiết bị
}

function requestNotificationsPermissions() {
	// TODO 11: Yêu cầu gửi thông báo nổi
}

// Hàm gửi ảnh
function onMediaFileSelected(event) {
	event.preventDefault();
	var file = event.target.files[0];

	// Xoá nọi dung của anh cũ
	imageFormElement.reset();

	// Kiểm tra xem file có được gửi ảnh lên không
	if (!file.type.match("image.*")) {
		var data = {
			message: "You can only share images",
			timeout: 2000
		};
		signInSnackbarElement.innerHTML = data.message;
		return;
	}
	// Kiểm tra người dùng đã đăng nhập chưa
	if (checkSignedInWithMessage()) {
		saveImageMessage(file);
	}
}

// Gửi tin nhắn
function onMessageFormSubmit(e) {
	e.preventDefault();
	// Kiểm tra nếu nhập tin nhắn rồi và đã đăng nhập thì gửi tin nhắn
	if (messageInputElement.value && checkSignedInWithMessage()) {
		saveMessage(messageInputElement.value).then(function () {
			// Xoá nội dung tin nhắn vừa gửi
			resetMaterialTextfield(messageInputElement);
			toggleButton();
		});
	}
}

function authStateObserver(user) {
	console.log(user);
	if (user) {
		var profilePicUrl = getProfilePicUrl();
		var userName = getUserName();
		var id = getEmailId();
		var userInfo = {
			id: id,
			userName: userName,
			email: getEmail(),
			avatar: profilePicUrl
		}
		updateUserOnline(id, userInfo);

		userPicElement.setAttribute('src', profilePicUrl);
		userNameElement.textContent = userName;

		userSignedInPanel.style.display = "block";
		signInButtonElement.style.display = "none";
		saveMessagingDeviceToken();
	} else {
		userSignedInPanel.style.display = "none";
		signInButtonElement.style.display = "block";
	}
}

// Trả về true nếu người dùng đã đăng nhập, false nếu chưa và hiển thị thông báo lỗi
function checkSignedInWithMessage() {
	// Trả về true nếu người dùng đã đăng nhập
	if (isUserSignedIn()) {
		return true;
	}

	// Hiển thị thông tin lỗi không gửi được tin nhắn
	var data = {
		message: "You must sign-in first",
		timeout: 2000
	};
	signInSnackbarElement.innerHTML = data.message;
	return false;
}

// Xoá nội dung tin nhắn vừa gửi
function resetMaterialTextfield(element) {
	element.value = "";
	element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
}

// Mẫu giao diện tin nhắn
var MESSAGE_TEMPLATE =
	'<div class="message-container">' +
	'<div class="spacing"><img class="pic"/></div>' +
	'<div><div class="message"></div>' +
	'<div class="date"></div></div>' +
	"</div>";

// Lấy link ảnh từ google từ các tham số kích thước của ảnh
function addSizeToGoogleProfilePic(url) {
	if (url.indexOf("googleusercontent.com") !== -1 && url.indexOf("?") === -1) {
		return url + "?sz=150";
	}
	return url;
}

// Ảnh loading
var LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif?a";

// Hiển thị danh sách tin nhắn cũ
function displayMessage(key, name, text, picUrl, imageUrl, id , date ) {
	var div = document.getElementById(key);
	// Nếu Element chưa tồn tại thì tạo một Element mới
	if (!div) {
		var container = document.createElement("div");
		container.innerHTML = MESSAGE_TEMPLATE;
		div = container.firstChild;
		div.setAttribute("id", key);
		messageListElement.appendChild(div);
	}
	if(id === getEmailId()){
		div.style.justifyContent = "flex-end";
	} else {
		div.style.justifyContent = "flex-start";
	}
	if (picUrl) {
		var picture = div.querySelector(".pic");
		picture.setAttribute("data-toggle", "tooltip");
		picture.setAttribute("title", name);
		picture.setAttribute('src', addSizeToGoogleProfilePic(picUrl));
	}
	// div.querySelector(".name").textContent = name;
	var messageElement = div.querySelector(".message");
	var dateElement = div.querySelector(".date");
	var currentTime = new Date().getTime();
	dateElement.innerHTML = timeDifference(currentTime, date ? date : currentTime);
	if (text) {
		// If the message is text.
		messageElement.textContent = text;
		// Replace all line breaks by <br>.
		messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, "<br>");
	} else if (imageUrl) {
		// If the message is an image.
		var image = document.createElement("img");
		image.addEventListener("load", function () {
			messageListElement.scrollTop = messageListElement.scrollHeight;
		});
		image.src = imageUrl + "&" + new Date().getTime();
		messageElement.innerHTML = "";
		messageElement.appendChild(image);
	}
	// Hiển thị thẻ
	setTimeout(function () {
		div.classList.add("visible");
	}, 1);
	// Cuộn để xem tin nhắn mới
	messageListElement.scrollTop = messageListElement.scrollHeight;
	messageInputElement.focus();
}

// Cho phép button gửi tin nhắn nhấn vào được
function toggleButton() {
	if (messageInputElement.value) {
		submitButtonElement.removeAttribute("disabled");
	} else {
		submitButtonElement.setAttribute("disabled", "true");
	}
}

// Kiểm tra firebase SDK đã được import vào chưa
function checkSetup() {
	if (
		!window.firebase ||
		!(firebase.app instanceof Function) ||
		!firebase.app().options
	) {
		window.alert("Bạn chưa import thư viện");
	}
}

// Kiểm tra firebase SDK đã được import vào chưa
checkSetup();

// Lấy các Element từ id
var messageListElement = document.getElementById("messages");
var messageFormElement = document.getElementById("message-form");
var messageInputElement = document.getElementById("message");
var submitButtonElement = document.getElementById("submit");
var imageButtonElement = document.getElementById("submitImage");
var imageFormElement = document.getElementById("image-form");
var mediaCaptureElement = document.getElementById("mediaCapture");
var userPicElement = document.getElementById("user-pic");
var userNameElement = document.getElementById("user-name");
var signInButtonElement = document.getElementById("sign-in");
var signOutButtonElement = document.getElementById("sign-out");
var signInSnackbarElement = document.getElementById("must-signin-snackbar");
var listUserOnline = document.getElementById("list-user-online");
var userSignedInPanel = document.getElementById("user-signed-in-panel");

// Sự kiện lưu tin nhắn khi kết thúc
messageFormElement.addEventListener("submit", onMessageFormSubmit);
// Sự kiện đăng xuất
signOutButtonElement.addEventListener("click", signOut);
// Sự kiện đăng nhập
signInButtonElement.addEventListener("click", signIn);

// Sự kiện nhập từ bàn phím và thay đổi tin nhắn
messageInputElement.addEventListener("keyup", toggleButton);
messageInputElement.addEventListener("change", toggleButton);

// Sự kiện tải lên ảnh
imageButtonElement.addEventListener("click", function (e) {
	e.preventDefault();
	mediaCaptureElement.click();
});
mediaCaptureElement.addEventListener("change", onMediaFileSelected);

// Khởi tạo xác thực firebase
initFirebaseAuth();

// Tải tin nhắn cũ
loadMessages();

// Chuyển đổi giờ 
function timeDifference(current, previous) {
	var msPerMinute = 60 * 1000;
	var msPerHour = msPerMinute * 60;
	var msPerDay = msPerHour * 24;
	var msPerMonth = msPerDay * 30;
	var msPerYear = msPerDay * 365;

	var elapsed = current - previous;

	if (elapsed < msPerMinute) {
		return Math.round(elapsed / 1000) + " seconds ago";
	} else if (elapsed < msPerHour) {
		return Math.round(elapsed / msPerMinute) + " minutes ago";
	} else if (elapsed < msPerDay) {
		return Math.round(elapsed / msPerHour) + " hours ago";
	} else if (elapsed < msPerMonth) {
		return "approximately " + Math.round(elapsed / msPerDay) + " days ago";
	} else if (elapsed < msPerYear) {
		return "approximately " + Math.round(elapsed / msPerMonth) + " months ago";
	} else {
		return "approximately " + Math.round(elapsed / msPerYear) + " years ago";
	}
}

$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();
});